const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
  constructor(repository) {
    this.repositoty = repository;
  }
  create(fight) {
    return this.repositoty.create(fight);
  }

  getFights() {
    const fights = this.repositoty.getAll();

    if (!fights) {
      return null;
    }
    return fights;
  }

  getFightsForUser(search) {
    const fights = this.repositoty.getMany(search);

    if (!fights) {
      return null;
    }
    return fights;
  }

  update(id, data) {
    const updatedFight = this.repositoty.update(id, data);

    if (!updatedFight) {
      return null;
    }
    return updatedFight;
  }

  remove(id) {
    const removedFight = this.repositoty.delete(id);

    if (!removedFight) {
      return null;
    }
    return removedFight;
  }

  search(search) {
    const item = this.repositoty.getOne(search);

    if (!item) {
      return null;
    }
    return item;
  }
}

module.exports = new FightersService(FightRepository);